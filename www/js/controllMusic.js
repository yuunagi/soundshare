function changeMusic(value){
    NowPlaying = value;
}

function playSound(file){
    if($("#"+file).get(0).paused){
        if(!NowPlaying){
            $("#"+file).get(0).play();
            changeMusic(file)
        }else{
            $("#"+NowPlaying).get(0).pause();
            $("#"+file).get(0).play();
            changeMusic(file)
        }
    }
    else{
        $("#"+file).get(0).pause();
        changeMusic("");
    }
}

