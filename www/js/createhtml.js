/*
function getResJson(){
    if(window.JSON){
        
        // ------------------------------------------------------------
        // JSON 文字列を用意する
        // ------------------------------------------------------------
        var json_text =
        '{\
        "post":{\
        "posted_user_name" : "nagisa",\
        "posted_user_id" : 1,\
        "posted_user_img_path" : "http://example.com/sample.jpg",\
        "comment" : "サンプル。サンプル。サンプル<a href=\'http://example.com/1\'>#タグ１</a><a href=\'http://example.com/2\'>#タグ２</a>",\
        "sound_url" : "http://153.121.71.239/typewild.mp3",\
        "favorited_num" : 100,\
        "play_num" : 10,\
        "comment_num" : 5,\
        "is_favorite" : true\
        }\
        }';
        
        // ------------------------------------------------------------
        // 「JSON 文字列」から「JavaScript のオブジェクト」に変換する
        // ------------------------------------------------------------
        var obj = JSON.parse(json_text);
        
        // テスト出力
        return obj
    }
}
 */
//ホーム画面の各行デザイン定義
function returnHomeElement(obj,index){
    console.log(soundId);
    var element = '\
<div>\
<a style="display:block" href="#list-item-detail" onclick="transition_list_item_detail(' + obj.posts[index].sound_id + ')">\
<div class="list_image">\
<img src="' + obj.posts[index].author.icon_image + '"width="100%">\
</div>\
<div class="list_property">\
<p class="heavy">' + obj.posts[index].author.name + '</p>\
<p class="heavy">'+ obj.posts[index].description + '</p>\
</div>\
</a>\
</div>\
<div class="list_play_button">\
<div class="ui-btn ui-input-btn ui-corner-all ui-shadow">\
<input type="button" onclick="playSound(\'' + soundId + '\')">\
<i class="fa fa-play"></i>\
</div>\
</div>\
<div class="list_reply_button">\
<div class="ui-btn ui-input-btn ui-corner-all ui-shadow">\
<input type="button" onclick="createTweet()">\
<i class="fa fa-star"></i>\
</div>\
</div>\
<div class="list_favorite_button">\
<div class="ui-btn ui-input-btn ui-corner-all ui-shadow">\
<input type="button">\
<i class="fa fa-undo"></i>\
</div>\
</div>\
<audio id="' + soundId + '" preload="auto">\
<source src="' + obj.posts[index].sound_url + '" type="audio/mp3">\
</audio>';
    soundId=soundId+1;
    return element;
}
//ホーム画面のデザイン挿入
function createHome(obj,index){
    /*DIVタグのノードオブジェクト*/
    var frameNode = document.getElementById('home_body');
    var flgmntNode = document.createDocumentFragment();
    /*DIVタグ要素を新たに生成*/
    var element =returnHomeElement(obj,index);
    var newDivNode = document.createElement('li');
    newDivNode.className ='ui-li-static ui-body-inherit ui-first-child';
    newDivNode.innerHTML = element;
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    flgmntNode.appendChild(newDivNode);
    
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    frameNode.appendChild(flgmntNode);
}


//リストアイテム画面の投稿音声情報メイン定義
function returnListItems(obj){
    var element = '\
    <div class="list-item-detail-topic">\
    <img src="' + obj.post.author.icon_image + '"class="list-item-detail-image">\
    <div>\
    <a class="list-item-detail-user-name">' + obj.post.author.name + '</a>\
    <a class="list-item-detail-post-time">' + obj.post.posted_at + '</a>\
    <br>\
    <p class="heavy">' + obj.post.description + '</p>\
    </div>\
    </div>\
    <div class="list-item-detail-play-button">\
    <button class="ui-btn ui-input-btn ui-corner-all ui-shadow" onclick="playSound(\'' + soundId + '\')">再生と時間</button>\
    </div>\
    <div class="list-item-detail-seekbar">\
    <button class="ui-btn ui-input-btn ui-corner-all ui-shadow">シークバーとボタン</button>\
    </div>\
    <hr class="emphasis">\
    <div>\
    <audio id="' + soundId + '" preload="auto">\
    <source src="' + obj.post.sound_url + '" type="audio/mp3">\
    </audio>';
    soundId=soundId+1;
    return element;
    
}

//リストアイテム画面のデザイン挿入
function createListItem(obj){
    /*DIVタグのノードオブジェクト*/
    var frameNode = document.getElementById('ListItem_body');
    var flgmntNode = document.createDocumentFragment();
    /*DIVタグ要素を新たに生成*/
    var element =returnListItems(obj);
    var newDivNode = document.createElement('div');
    newDivNode.innerHTML = element;
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    flgmntNode.appendChild(newDivNode);
    
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    frameNode.appendChild(flgmntNode);
}

//リストアイテム画面のコメント定義
function returnListItemComment(obj,index){
    var element = '\
    <div>\
    <div class="list-item-detail-comment-attribute">\
    <p>CorS</p>\
    </div>\
    <div class="list-item-detail-comment-field">\
    <div class="list-item-detail-topic">\
    <img src="/Users/nagisayuuhe/Desktop/myprojects/phonegap/www/img/logo.png"class="list-item-detail-image">\
    <div>\
    <a class="list-item-detail-user-name">' + obj.post.comments[index].author.name + '</a>\
    <a class="list-item-detail-post-time">' + obj.post.comments[index].commented_at + '</a>\
    <br>\
    <p class="heavy">' + obj.post.comments[index].comment + '</p>\
    </div>\
    </div>\
    </div>\
    </div>\
    <hr class="emphasis">';
    return element;
}

//リストアイテム画面のコメント情報挿入
function createListItemComment(obj,index){
    /*DIVタグのノードオブジェクト*/
    var frameNode = document.getElementById('ListItem_comment');
    var flgmntNode = document.createDocumentFragment();
    /*DIVタグ要素を新たに生成*/
    var element =returnListItemComment(obj,index);
    var newDivNode = document.createElement('div');
    newDivNode.innerHTML = element;
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    flgmntNode.appendChild(newDivNode);
    
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    frameNode.appendChild(flgmntNode);
}

//ユーザプロファイル画面のユーザメイン情報定義
function returnUseProfile(obj,index){
    var element = '\
<div class="user-profile-topic">\
    <div class="user-profile-image-area">\
        <img src="/Users/nagisayuuhe/Desktop/myprojects/phonegap/www/img/logo.png"class="user-profile-image">\
        <p class="heavy" style="text-align:center;">君</p>\
    </div>\
    <div　class="user-profile-text-area">\
        <p class="heavy">投稿音声数</p>\
        <p class="heavy">いいねされた数</p>\
        <p class="heavy">フォロー数</p>\
        <p class="heavy">フォロワー数</p>\
        <div class="follow_button">\
        <div class="ui-btn ui-input-btn ui-corner-all ui-shadow">follow<input type="button" value="follow"></div>\
    </div>\
</div>\
<hr class="emphasis">';
    return element;
}

//ユーザプロファイル画面のユーザメイン情報挿入
function createUserProfile(obj){
    /*DIVタグのノードオブジェクト*/
    var frameNode = document.getElementById('UserProfile_body');
    var flgmntNode = document.createDocumentFragment();
    /*DIVタグ要素を新たに生成*/
    var element =returnUseProfile(obj);
    var newDivNode = document.createElement('div');
    newDivNode.innerHTML = element;
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    flgmntNode.appendChild(newDivNode);
    
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    frameNode.appendChild(flgmntNode);
}

//ユーザプロファイル画面の投稿音声情報定義
function returnUseProfilePostedSound(obj,index){
    var element = '\
    <div class="list_image">\
    <img src="/Users/nagisayuuhe/Desktop/myprojects/phonegap/www/img/logo.png"width="100%">\
    </div>\
    <div class="list_property">\
    <p class="heavy">最後かもしれないだろ</p>\
    <p class="heavy">だから、</p>\
    <p class="heavy">全部話しておきたいんだ</p>\
    </div>\
    <div class="list_play_button">\
    <div class="ui-btn ui-input-btn ui-corner-all ui-shadow">play<input type="button" value="play"></div>\
    </div>\
    <div class="list_reply_button">\
    <div class="ui-btn ui-input-btn ui-corner-all ui-shadow">play<input type="button" value="play"></div>\
    </div>\
    <div class="list_favorite_button">\
    <div class="ui-btn ui-input-btn ui-corner-all ui-shadow">play<input type="button" value="play"></div>\
    </div>';
    return element;
}


//ユーザプロファイル画面の投稿音声情報挿入
function createUserProfilePostedSound(obj){
    /*DIVタグのノードオブジェクト*/
    var frameNode = document.getElementById('UserProfile_PostedSound');
    var flgmntNode = document.createDocumentFragment();
    /*DIVタグ要素を新たに生成*/
    var element =returnUseProfilePostedSound(obj);
    var newDivNode = document.createElement('li');
    newDivNode.className ='ui-li-static ui-body-inherit ui-first-child';
    newDivNode.innerHTML = element;
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    flgmntNode.appendChild(newDivNode);
    
    /*frameNodeに、新たに生成したDIVタグ要素を追加*/
    frameNode.appendChild(flgmntNode);
}